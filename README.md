# Calibration
Repositorio desarrollado en python para calibrar los test de estudio que de ejecutaran en SW2D-LEMON.

Desarrollado dentro de la tesis de Joseph Luis Kahn Casapia


# Estructura

    * data
    * mesh
    * res
    * src
    * test
    * utils
