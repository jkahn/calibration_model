# calibrar por partes
import re

patron = re.compile(r'\s+')

dir_name = '../test/Calibration_medium/input/'
path_lim = dir_name + 'Taihu_backup.lim'
pathfile = dir_name + 'prueba.lim'

m1_6_in = 9.93866656e-01
m1_6_ou = 5.45077183e-01

m7_12_in = 3.64927791e-01
m7_12_ou = 3.38913424e-01

time = 15552000

with open(path_lim,'r') as f:
    print('> file .lim opened')
    lines = f.readlines()
    nbc = int(lines[2])
    for j in range(8,len(lines)):
        line = [float(val) for val in patron.split(lines[j])[:-1]]

        if time >= line[0]:
            theta_in = m1_6_in
            theta_out = m1_6_ou
        else :
            theta_in = m7_12_in
            theta_out = m7_12_ou

        for i in range(2,nbc+1):
            if line[i] > 0:
                line[i] = theta_in * line[i]
            elif line[i] < 0:
                line[i] = theta_out * line[i]
        line = ' '.join([str(val) for val in line])  + '\n'
        lines[j] = line

    lines = ''.join(lines)

    with open(pathfile, 'w') as g:
        g.write(lines)
        print('> file .lim updated')
