from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy import interpolate
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Proj


def main():
    bathy_name = "bathy_taihu.txt"
    nodes_name = "Vertices.txt"
    trian_name = "Triangles.txt"
    bathy = np.loadtxt(bathy_name) 
    ver = np.loadtxt(nodes_name)
    tri = np.loadtxt(trian_name)[:,1:]

    myProj = Proj(proj='utm', zone=51)
    lon, lat = myProj(ver[:,0], ver[:,1], inverse=True)
    
    # Interpolation
    opc = 0
    if opc == 0:
        met = 'nearest'
    elif opc == 1:
        met = 'linear'
    elif opc == 2:
        met = 'cubic'

    z = interpolate.griddata(bathy[:,1:], bathy[:,0], (lon,lat), method=met)
    ver[:,2] = 4-z
    #np.savetxt(nodes_name,ver)


    Cen3D = plt.figure()
    ax = Cen3D.gca(projection='3d')

    #ax.scatter(bathy[:,1],bathy[:,2],-bathy[:,0],cmap='hot',c=bathy[:,0])
    #ax.scatter(bathy_xyz[:,0],bathy_xyz[:,1],-bathy_xyz[:,2],cmap='hot',c=bathy[:,0])
    ax.plot(lon,lat,4-z,'b^')
    #ax.plot(bathy[:,1],bathy[:,2],'r^')
    #ax.plot(bathy_xyz[:n,0],bathy_xyz[:n,1],bathy_xyz[:n,2],'r^')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

    np.savetxt(nodes_name,ver)

    name  = "Taihu"
    f = open(name+".2dm","w")
    texto = "MESH2D\n"

    ntri = len(tri)
    for i in range(ntri):
        texto += 'E3T %i %i %i %i 1\n'%(i+1,tri[i,0],tri[i,1],tri[i,2])

    for i in range(len(ver)):
        texto += 'ND %i %f %f %f\n'%(i+1,ver[i,0],ver[i,1],ver[i,2])
    texto += """BEGPARAMDEF
GM  "Mesh"
SI  1
DY  0
TU  ""
TD  0  0
NUME  3
BCPGC  0
BEDISP  0 0 0 0 1 0 1 0 0 0 0 1
BEFONT  0 2
BEDISP  1 0 0 0 1 0 1 0 0 0 0 1
BEFONT  1 2
BEDISP  2 0 0 0 1 0 1 0 0 0 0 1
BEFONT  2 2
ENDPARAMDEF
BEG2DMBC
MAT  1 "material 01"
END2DMBC"""
    f.write(texto)
    f.close()
main()
