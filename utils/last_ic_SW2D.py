import numpy as np
import pandas as pd
import os
import sys
import argparse

sys.path.append('..')
import src.show_stations as ss


# parse
"""
parser = argparse.ArgumentParser(description='flag to open the last values')

parser.add_argument('-e',
                    '--extension',
                    dest='ext',
                    action='store_true',
                    required=True,
                    help='extension of the result (ext or vtk)')

args = parser.parse_args()

print(args.ext)
"""
t_end = 31536000

res_file = 'ResultHydro_'
res_file = 'Biological_Taihu_'
name_csv = 'Taihu_bio'
csv_ex = 'csv'

ext = 'csv'
#ext ='vtk'


def exp_form(t):
    lis_exp = ('%6e'%t_end).split('e+')
    lis_exp[1] = '%i'%int(lis_exp[1])
    a_exp = 'e+'.join(lis_exp)
    return a_exp


# automatic generation of file name
def generate_file_name(dir_simu):
    # folder output
    if ext == 'csv':
        list_dir = os.listdir(os.sep.join(['.' + dir_simu, ss.oput]))
        list_dir = [name for name in list_dir if (name_csv in name) and (ext in name)]
        indx_dir = [float(f.split(name_csv+'_')[-1].split('.'+ext)[0]) for f in list_dir]
        zip_list = zip(indx_dir, list_dir)
        sor_list = sorted(zip_list)
    # folder misc
    if ext == 'vtk':
        list_dir = os.listdir(os.sep.join(['.' + dir_simu, ss.misc]))
        list_dir = [name for name in list_dir if (res_file in name) and (ext in name)]
        indx_dir = [float(f.split(res_file+'frame_')[-1].split('_layer_0.'+ext)[0]) for f in list_dir]
        zip_list = zip(indx_dir, list_dir)
        sor_list = sorted(zip_list)
    return sor_list[-1][-1]

def main():
    dir_simu = ss.dir_simu
    dir_simu = './test/Calibration_bio_small'
    file_name = generate_file_name(dir_simu)
    if ext == 'csv':
        df_ini = ss.openCSV(os.sep.join(['.'+dir_simu, ss.oput, file_name]))
    elif ext == 'vtk':
        df_ini = ss.openVtkFile(os.sep.join(['.'+dir_simu, ss.misc, file_name]))
    df_ini.to_csv("output.ini", columns=['heat','concentration1', 'concentration2', 'concentration3'], sep='\t', index=False, header=None)
    #df_ini.to_csv("output.ini", columns=['surface_elevation', 'x_unit_discharge', 'y_unit_discharge'], sep='\t', index=False, header=None)

main()
