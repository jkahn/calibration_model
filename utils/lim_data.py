import datetime as dt
import numpy as np
import os

node_to_river = {
    3741 : "Dapu_River",
    2297 : "Sanliqiao",
    535 : "Cao&huangyan",
    2152 : "Wulihuzha",
    3451 : "Wangyu_River",
    2086 : "Dushanzha",
    3160 : "Huguang_Canal",
    2683 : "Gulougang",
    3178 : "Huazhuang&Zhonghua",
    105 : "Chendonggang",
    334 : "Changxinggang",
    1760 : "Dongtiaoxi",
    1239 : "Xitiaoxi",
    3621 : "Xinyunhe_Canal",
    3619 : "Xujiang_River",
    1302 : "Wujingang",
    3630 : "Taipu_River"
}

river_to_node = {
    "Dapu_River" : 2,
    "Sanliqiao" : 3,
    "Cao&huangyan" : 4,
    "Wulihuzha" : 5,
    "Wangyu_River" : 6,
    "Dushanzha" : 5,
    "Huguang_Canal" : 7,
    "Gulougang" : 8,
    "Huazhuang&Zhonghua" : 9,
    "Chendonggang" : 10,
    "Changxinggang" : 11,
    "Dongtiaoxi" : 12,
    "Xitiaoxi" : 13,
    "Xinyunhe_Canal" : 14,
    "Xujiang_River" : 15,
    "Wujingang" : 16,
    "Taipu_River" : 17
}

filename_riv = "/home/joseph/Datos_Taihu/Data/rivers_LakeTaihu_dat.txt"
filename_sta = "/home/joseph/Datos_Taihu/Data/obs_data_LakeTaihu_dat.txt"

dirname_sav = "/home/joseph/Datos_Taihu/Data_sw2d"

cond_NH4 = np.zeros([365,18])
cond_NO3 = np.zeros([365,18])
cond_ON = np.zeros([365,18])
cond_IP = np.zeros([365,18])
cond_OP = np.zeros([365,18])
reference = dt.datetime(2001,5,1,0,0,0)

head = """#discPor
#bcs
17
#
# each row is to a transport component
q c c c c c c c c c c c c c c c c c
#
#
"""


# Open data
with open(filename_riv) as f:
    lines = f.readlines()

    for i in range(len(lines)):
        lines[i] = lines[i].split("\n")[0]
        lines[i] = lines[i].split(" ")
        if i > 1:
            lines[i][6] = float(lines[i][6])
            lines[i][7] = float(lines[i][7])
            lines[i][8] = float(lines[i][8])
            lines[i][9] = float(lines[i][9])
            lines[i][10] = float(lines[i][10])
            lines[i][11] = float(lines[i][11])


    for i in range(2,len(lines)):
        if lines[i][0] in river_to_node.keys():
            index = river_to_node[lines[i][0]]
            date = lines[i][1].split("-")[0].split("/")
            date = [int(date[i]) for i in range(len(date)-1,-1,-1)]

            hour = lines[i][1].split("-")[1].split(":")
            hour = [int(x) for x in hour]

            days = (dt.datetime(date[0],date[1],date[2],hour[0],hour[1],hour[2]) - reference).days
            totalseg = 86400 * days

            cond_NH4[days][0] = totalseg
            cond_NH4[days][index] = lines[i][6]


            cond_NO3[days][0] = totalseg
            cond_NO3[days][index] = lines[i][7] + lines[i][8]

            cond_ON[days][0] = totalseg
            if lines[i][9] - lines[i][8] - lines[i][7] - lines[i][6] < 0:
                cond_ON[days][index] = 0
            else :
                cond_ON[days][index] = lines[i][9] - lines[i][8] - lines[i][7] - lines[i][6]

            cond_IP[days][0] = totalseg
            cond_IP[days][index] = lines[i][11]

            cond_OP[days][0] = totalseg
            if lines[i][10] - lines[i][11] < 0:
                cond_OP[days][index] = 0
            else :
                cond_OP[days][index] = lines[i][10] - lines[i][11]

with open(dirname_sav + os.sep + "Taihu_NH4.lim",'w') as file_NH4:
    file_NH4.write(head)
    for i in range(cond_NH4.shape[0]):
        aux_line = [str(x) for x in cond_NH4[i]]
        file_NH4.write(" ".join(aux_line) + "\n")

with open(dirname_sav + os.sep + "Taihu_NO3.lim","w") as file_NO3:
    file_NO3.write(head)
    for i in range(cond_NO3.shape[0]):
        aux_line = [str(x) for x in cond_NO3[i]]
        file_NO3.write(" ".join(aux_line) + "\n")

with open(dirname_sav + os.sep + "Taihu_ON.lim","w") as file_ON:
    file_ON.write(head)
    for i in range(cond_ON.shape[0]):
        aux_line = [str(x) for x in cond_ON[i]]
        file_ON.write(" ".join(aux_line) + "\n")

with open(dirname_sav + os.sep + "Taihu_IP.lim","w") as file_IP:
    file_IP.write(head)
    for i in range(cond_IP.shape[0]):
        aux_line = [str(x) for x in cond_IP[i]]
        file_IP.write(" ".join(aux_line) + "\n")

with open(dirname_sav + os.sep + "Taihu_OP.lim","w") as file_OP:
    file_OP.write(head)
    for i in range(cond_OP.shape[0]):
        aux_line = [str(x) for x in cond_OP[i]]
        file_OP.write(" ".join(aux_line) + "\n")
