#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.4.0 with dump python functionality
###
import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/user/jkahncas/home')

import os
import sys
sys.path.insert(0, r'/home/jkahncas/Development/calibration')

ver = []
with open('/home/jkahncas/Development/calibration/mesh/taihu_small/Vertices.txt') as f:
    lines = f.readlines()
    for line in lines:
        values = line.split('\n')[0].split(' ')
        if len(values) == 3:
            values = [float (value) for value in values]
            ver.append(values)

tri = []
with open('/home/jkahncas/Development/calibration/mesh/taihu_small/Triangles.txt') as f:
    lines = f.readlines()
    for line in lines:
        values = line.split('\n')[0].split(' ')
        if len(values) == 4:
            values = [int(value) for value in values]
            tri.append(values)


lados = []
for t in tri:
    a = [t[1],t[2]]
    b = [t[2],t[3]]
    c = [t[3],t[1]]








###
### GEOM component
###
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)

OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

Vertex_1 = geompy.MakeVertex(0, 0, 0)
Vertex_2 = geompy.MakeVertex(1, 0, 0)

#Line_1 = geompy.MakeLineTwoPnt(Vertex_2, Vertex_1)

#Adding the nodes
vertices = []
for i in range(len(ver)):
    vertices.append(geompy.MakeVertex(ver[i][0], ver[i][1], 0))
    geompy.addToStudy(vertices[i] , 'Vertex_%i'%(i+1))

#Adding the edges
lineas = []
for i in range(len(tri)):
    t = tri[i]
    for j in range(len(t)):
        lineas.append(geompy.MakeLineTwoPnt(vertices[], Vertex_1))
#geompy.addToStudy( Vertex_1, 'Vertex_1' )
#geompy.addToStudy( Vertex_2, 'Vertex_2' )

#geompy.addToStudy( Line_1, 'Line_1' )

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
