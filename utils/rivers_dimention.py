import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import os

nriv = 17

head = '''#discPor
#bcs
%i
#
# each row is to a river
q q q q q q q q q q q q q q q q q
#
#
'''%nriv

node_to_river = {
    3741 : ['Dapu_River',173],
    2297 : ['Sanliqiao',94],
    535 : ['Cao&huangyan',60],
    2152 : ['Wulihuzha',39],
    3451 : ['Wangyu_River',162],
    2086 : ['Dushanzha',39],
    3160 : ['Huguang_Canal',168],
    2683 : ['Gulougang', 96],
    3178 : ['Huazhuang&Zhonghua',24],
    105 : ['Chendonggang',68],
    334 : ['Changxinggang',82],
    1760 : ['Dongtiaoxi',91],
    1239 : ['Xitiaoxi',89],
    3621 : ['Xinyunhe_Canal',104],
    3619 : ['Xujiang_River',136],
    1302 : ['Wujingang',44],
    3630 : ['Taipu_River',105]
}

river_to_node = {
    'Dapu_River' : 2,
    'Sanliqiao' : 3,
    'Cao&huangyan' : 4,
    'Wulihuzha' : 5,
    'Wangyu_River' : 6,
    'Dushanzha' : 5,
    'Huguang_Canal' : 7,
    'Gulougang' : 8,
    'Huazhuang&Zhonghua' : 9,
    'Chendonggang' : 10,
    'Changxinggang' : 11,
    'Dongtiaoxi' : 12,
    'Xitiaoxi' : 13,
    'Xinyunhe_Canal' : 14,
    'Xujiang_River' : 15,
    'Wujingang' : 16,
    'Taipu_River' : 17
}

dir_name = '/home/jkahncas/Development/calibration/'

dirname_mesh = dir_name + 'mesh/taihu_small/'
filename_inf = 'riv_info_small.txt'
filename_ver = 'Vertices.txt'
filename_tri = 'Triangles.txt'
filename_riv = dir_name + 'data/rivers_LakeTaihu_dat.txt'
#filename_sta = '/home/joseph/Datos_Taihu/Data/obs_data_LakeTaihu_dat.txt'

dirname_sav = dir_name + 'lim_small.lim'
# Open mesh : vertices and triangles
ver = np.loadtxt(dirname_mesh + filename_ver)
tri = np.loadtxt(dirname_mesh + filename_tri, dtype=int)[:,1:] - 1

# Init figure
fig = plt.figure()

# Open info Taihu / Taihu Small
conection = {}
name2node = {}
with open(dirname_mesh + filename_inf) as f:
    lines = f.readlines()
    aux = 0
    for line in lines:
        if 'River' in line:
            aux = int(line.split('\n')[0].split(' ')[-1])
            #print(node_to_river[aux])
        else:
            a,b = line.split('\n')[0].split(' ')
            a = int(a)
            b = int(b)

            plt.plot(ver[a-1,0],ver[a-1,1],'r^')
            plt.plot(ver[b-1,0],ver[b-1,1],'r^')

            d = ((ver[a-1,0] - ver[b-1,0])**2 + (ver[a-1,1] - ver[b-1,1])**2)**0.5
            #print(d , node_to_river[aux][1] , node_to_river[aux][1]/d)
            conection[aux] = [a, b, node_to_river[aux][1], d, node_to_river[aux][1]/d]
            name2node[node_to_river[aux][0]] = aux

for i in conection.keys():
    print(conection[i])

# Open river flux data
df_riv_flux = pd.read_csv(filename_riv, sep=' ', index_col=False)
df_riv_flux = df_riv_flux.drop(index=0)

df_riv_flux['Date'] = pd.to_datetime(df_riv_flux['Date'], format='%d/%m/%Y-%H:%M:%S')
df_riv_flux['flux'] = pd.to_numeric(df_riv_flux['flux'])
df_riv_flux['temp'] = pd.to_numeric(df_riv_flux['temp'])
df_riv_flux['salt'] = pd.to_numeric(df_riv_flux['salt'])
df_riv_flux['CODMn'] = pd.to_numeric(df_riv_flux['CODMn'])
df_riv_flux['NH4'] = pd.to_numeric(df_riv_flux['NH4'])
df_riv_flux['NO2'] = pd.to_numeric(df_riv_flux['NO2'])
df_riv_flux['NO3'] = pd.to_numeric(df_riv_flux['NO3'])
df_riv_flux['TN'] = pd.to_numeric(df_riv_flux['TN'])
df_riv_flux['TP'] = pd.to_numeric(df_riv_flux['TP'])
df_riv_flux['PO4'] = pd.to_numeric(df_riv_flux['PO4'])

#print(river_to_node.keys())

# build output
cond_rive = np.zeros([365,18])
reference = dt.datetime(2001,5,1,0,0,0)

df_riv_flux['diff'] = (df_riv_flux['Date'] - reference).dt.total_seconds().astype(int)
for name in name2node.keys():
    cond_rive[:,0] = df_riv_flux[df_riv_flux['Name_river'] == name][['diff']].values[:,0]
    cond_rive[:,river_to_node[name]] += (df_riv_flux[df_riv_flux['Name_river'] == name][['flux']] / conection[name2node[name]][3]).values[:,0]

for i in range(cond_rive.shape[0]):
    head += ' '.join(map(str, cond_rive[i])) + '\n'

# save output
with open(dirname_sav, 'w') as f:
    f.write(head)

# Plot mesh
plt.triplot(ver[:,0], ver[:,1], tri)

# show mesh
plt.show()
