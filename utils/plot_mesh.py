import os
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
#import matplotlib.tri as tri
from mpl_toolkits.mplot3d import Axes3D

def OpenMesh(name_n, name_t):
    Ver = []
    f = open(name_n,'r')
    f_lines = f.readlines()
    f_lines = f_lines[1:]
    for j in range(len(f_lines)):
        f_line = f_lines[j]
        f_line = f_line.split('\n')
        f_line = f_line[0]
        f_line = f_line.split(' ')
        f_line = [float(i) for i in f_line]
        Ver += [f_line]
    f.close()

    Tri = []
    g = open(name_t,'r')
    g_lines = g.readlines()
    g_lines = g_lines[1:]
    for j in range(len(g_lines)):
        g_line = g_lines[j]
        g_line = g_line.split('\n')
        g_line = g_line[0]
        g_line = g_line.split(' ')
        g_line = [int(i) for i in g_line]
        Tri += [g_line[1:]]
    g.close()
    return np.asarray(Ver), np.asarray(Tri)-1

def openBoundary(path_file):
    boun = []
    with open(path_file,"r") as f:
        nline = 0
        line = f.readline()
        ntotal = int(line)
        while nline < ntotal:
            line = f.readline()
            line = line.split("\n")[0].split(" ")[:2]
            boun.append([int(x)-1 for x in line])
            nline += 1
    return np.asarray(boun)

def main():
    dirname_mesh = "/home/jkahncas/Development/calibration/mesh"

    dirname_case = "taihu_medium"

    name_ver1 = os.sep.join([dirname_mesh, dirname_case, 'v.txt'])
    name_tri1 = os.sep.join([dirname_mesh, dirname_case, 't.txt'])
    boun1 = openBoundary(os.sep.join([dirname_mesh, dirname_case, 'b.txt']))

    dirname_case = "taihu/"
    name_ver2 = os.sep.join([dirname_mesh, dirname_case, 'Vertices.txt'])
    name_tri2 = os.sep.join([dirname_mesh, dirname_case, 'Triangles.txt'])

    Ver1, Tri1 = OpenMesh(name_ver1,name_tri1)
    Ver2, Tri2 = OpenMesh(name_ver2,name_tri2)

    fig, ax = plt.subplots(1,1)

    #ax.triplot(Ver2[:,0], Ver2[:,1], Tri2, 'b.-', lw=0.1)
    ax.triplot(Ver1[:,0], Ver1[:,1], Tri1, '-', lw=1.5)
    #print(boun1[0,0])
    for i in range(16):
        ax.plot(Ver1[boun1[i,:],0], Ver1[boun1[i,:],1], 'ro-')
        #ax.plot(Ver1[boun1[i,1],0], Ver1[boun1[i,1],1], 'ro')
        ax.text(Ver1[boun1[i,0],0], Ver1[boun1[i,0],1], str(i))
    plt.show()

main()
