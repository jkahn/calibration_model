import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if sys.platform.startswith('linux'):
    # Linux-specific code here...
    path_dirname = "/home/jkahncas/Development/calibration/"
    path_dirname = "/home/joseph/Develop/calibration/"

sys.path.insert(0, path_dirname)

import src.show_stations as ss

mesh_dirname = 'mesh'
mesh_case = 'taihu_medium'
mesh_case_r = 'taihu'

riv_filename = "longlat_rivers_loc_LakeTaihu.txt"

node_to_river = {
    3741 : "Dapu_River",
    2297 : "Sanliqiao",
    535 : "Cao&huangyan",
    2152 : "Wulihuzha",
    3451 : "Wangyu_River",
    2086 : "Dushanzha",
    3160 : "Huguang_Canal",
    2683 : "Gulougang",
    3178 : "Huazhuang&Zhonghua",
    105 : "Chendonggang",
    334 : "Changxinggang",
    1760 : "Dongtiaoxi",
    1239 : "Xitiaoxi",
    3621 : "Xinyunhe_Canal",
    3619 : "Xujiang_River",
    1302 : "Wujingang",
    3630 : "Taipu_River"
}

node_to_ad = {
    3741 : 3740,
    2297 : 2224,
    535  : 564,
    2152 : 2205,
    3451 : 3442,
    2086 : 2152,
    3160 : 3121,
    2683 : 2616,
    3178 : 3224,
    105  : 122,
    334  : 322,
    1760 : 1685,
    1239 : 1194,
    3621 : 3599,
    3619 : 3640,
    1302 : 1362,
    3630 : 3621,
}

river_to_node = {
    "Dapu_River" : 3741,
    "Sanliqiao" : 2297,
    "Cao&huangyan" : 535,
    "Wulihuzha" : 2152,
    "Wangyu_River" : 3451,
    "Dushanzha" : 2086,
    "Huguang_Canal" : 3160,
    "Gulougang" : 2683,
    "Huazhuang&Zhonghua" : 3178,
    "Chendonggang" : 105,
    "Changxinggang" : 334,
    "Dongtiaoxi" : 1760,
    "Xitiaoxi" : 1239,
    "Xinyunhe_Canal" : 3621,
    "Xujiang_River" : 3619,
    "Wujingang" : 1302,
    "Taipu_River" : 3630
}

River_names = {
    1: "Dapu_River",
    2: "Sanliqiao",
    3: "Cao&huangyan",
    4: "Wulihuzha",
    5: "Wangyu_River",
    6: "Dushanzha",
    7: "Huguang_Canal",
    8: "Gulougang",
    9: "Huazhuang&Zhonghua",
    10: "Chendonggang",
    11: "Changxinggang",
    12: "Dongtiaoxi",
    13: "Xitiaoxi",
    14: "Xinyunhe_Canal",
    15: "Xujiang_River",
    16: "Wujingang",
    17: "Taipu_River"
}

def identifyBoundary(ver, tri, cen, ver_r, tri_r, cen_r, ax):
    boundary = []
    Tboundary = []
    ncells = tri.shape[0]
    for i in range(len(River_names)):
        node1 = river_to_node[River_names[i+1]] - 1
        node2 = node_to_ad[node1 + 1] - 1

        x_r = 0.5 * (ver_r[node1,0] + ver_r[node2,0])
        y_r = 0.5 * (ver_r[node1,1] + ver_r[node2,1])
        ax.plot(x_r, y_r, 'bo')
        ax.text(x_r, y_r, str(i+1))

        i_min = 0
        d_min = np.sqrt( (cen[i_min,0] - x_r)**2+ (cen[i_min,1] -y_r)**2 )
        for j in range(ncells):
            d = np.sqrt( (cen[j,0] - x_r)**2+ (cen[j,1] - y_r)**2 )
            if d < d_min:
                #if j<100:
                #print(node1, node2, j,d,i_min,d_min , d < d_min)
                i_min = j
                d_min = d
        boundary.append(i_min)
    return np.asarray(boundary)

def main():
    ver_r, tri_r = ss.openMesh(os.sep.join([path_dirname, mesh_dirname, mesh_case_r]))
    cen_r = ss.generateCenter(ver_r, tri_r)

    ver, tri = ss.openMesh(os.sep.join([path_dirname, mesh_dirname, mesh_case]))
    cen = ss.generateCenter(ver, tri)

    fig4, ax = plt.subplots()

    #print(cen.shape)
    bou = identifyBoundary(ver, tri, cen, ver_r, tri_r, cen_r,ax)

    #ax.plot(cen[:,0], cen[:,1], 'go')

    ax.triplot(ver[:,0], ver[:,1], tri[:,1:])
    for i in bou:
        ax.plot(cen[i,0], cen[i,1], 'r^' )
        ax.text(cen[i,0], cen[i,1], str(i + 1))
        print(i, tri[i,1:])
        for j in tri[i,1:]:
            ax.plot(ver[j,0],ver[j,1], 'g^')
            ax.text(ver[j,0],ver[j,1], str(j + 1))

    ax.plot(cen[95,0], cen[95,1], 'r^', )
    for j in tri[95,1:]:
        ax.plot(ver[j,0],ver[j,1], 'g^')
        ax.text(ver[j,0],ver[j,1], str(j + 1))
    plt.show()

main()
