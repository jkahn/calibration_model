import os
import re
import sys
import argparse
import vtk
import datetime as dt
import matplotlib.pyplot as plt


sys.path.append('..')
import src.show_stations as ss

parser = argparse.ArgumentParser(description='Render simulation result.')
parser.add_argument('-d', '--dirname', type=str, default='.', help='directory name')

args = parser.parse_args()

res_file = 'Biological_Taihu_'
csv_ex = 'csv'

ext = 'vtk'

if args.dirname == '.':
    args.dirname = '.' + ss.dir_simu

def generate_file_name(path_folder_name):
    list_dir = os.listdir(path_folder_name)
    list_dir = [name for name in list_dir if (res_file in name) and ('.vtp' in name)]
    indx_dir = [ int(f.split(res_file+'frame_')[-1].split('_layer_0.vtp')[0]) for f in list_dir]
    zip_list = zip(indx_dir, list_dir)
    sor_list =  sorted(zip_list)
    return [os.sep.join([path_folder_name, pair[-1]]) for pair in sor_list]

def main():
    # Mesh
    ver, tri = ss.openMesh(os.sep.join(['../'+ss.dir_mesh, ss.mesh_name]))


    vtkpath = os.sep.join([args.dirname, ss.misc])
    fileslist = generate_file_name(vtkpath)
    fig, ax = plt.subplots()

    for filename in fileslist[::50]:
        print(filename)
        ax.clear()
        df_data = ss.openVtkFile(filename)
        img = ax.tripcolor(ver[:,0], ver[:,1], tri[:,1:], facecolors=df_data['concentration1'], edgecolors='k',shading='flat')
        fig.colorbar(img)
        plt.pause(0.001)

main()
