from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy import interpolate
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Proj


def main():
    bathy_name = "bathy_taihu.txt"
    nodes_name = "v.txt"
    trian_name = "t.txt"
    bathy = np.loadtxt(bathy_name)
    ver = np.loadtxt(nodes_name)
    tri = np.loadtxt(trian_name)[:,1:] - 1

    myProj = Proj(proj='utm', zone=51)
    lon, lat = myProj(ver[:,0], ver[:,1], inverse=True)
    corx, cory =  myProj(bathy[:,1], bathy[:,2])
    print(len(lon), lon)

    asd = np.zeros([len(corx),3])
    asd[:,0] = corx
    asd[:,1] = cory
    asd[:,2] = bathy[:,0]
    np.savetxt("asd.txt",asd)

    # Interpolation
    opc = 0
    if opc == 0:
        met = 'nearest'
    elif opc == 1:
        met = 'linear'
    elif opc == 2:
        met = 'cubic'

    z = interpolate.griddata(bathy[:,1:], bathy[:,0], (lon,lat), method=met)
    ver[:,2] = 4-z
    #np.savetxt(nodes_name,ver)


    Cen3D = plt.figure()
    ax = Cen3D.gca(projection='3d')

    #ax.scatter(bathy[:,1],bathy[:,2],-bathy[:,0],cmap='hot',c=bathy[:,0])
    #ax.scatter(bathy_xyz[:,0],bathy_xyz[:,1],-bathy_xyz[:,2],cmap='hot',c=bathy[:,0])
    ax.plot(lon,lat,4-z,'b^')
    #ax.plot(bathy[:,1],bathy[:,2],'r^')
    #ax.plot(bathy_xyz[:n,0],bathy_xyz[:n,1],bathy_xyz[:n,2],'r^')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

    np.savetxt(nodes_name,ver)
main()
