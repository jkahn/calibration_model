import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import sys
from mpl_toolkits.mplot3d import Axes3D
from scipy import interpolate

sys.path.append('..')
import src.show_stations as ss


def InitialCondition(inNod, inVal, outNod):

    interpo = np.zeros([outNod.shape[0],4])

    interpo[:,0] = interpolate.griddata(inNod, inVal[:,0], outNod, method="cubic")
    interpo[:,1] = interpolate.griddata(inNod, inVal[:,1], outNod, method="cubic")
    interpo[:,2] = interpolate.griddata(inNod, inVal[:,2], outNod, method="cubic")
    interpo[:,3] = interpolate.griddata(inNod, inVal[:,3], outNod, method="cubic")


    condition = [False if np.isnan(interpo[i,3]) else True for i in range(len(interpo[:,3]))]
    valores = np.zeros([np.sum(condition),4])
    puntos = outNod[condition,:]

    valores[:,0] = interpo[condition,0]
    valores[:,1] = interpo[condition,1]
    valores[:,2] = interpo[condition,2]
    valores[:,3] = interpo[condition,3]

    interpo[:,0] = interpolate.griddata(puntos, valores[:,0], outNod, method="nearest")
    interpo[:,1] = interpolate.griddata(puntos, valores[:,1], outNod, method="nearest")
    interpo[:,2] = interpolate.griddata(puntos, valores[:,2], outNod, method="nearest")
    interpo[:,3] = interpolate.griddata(puntos, valores[:,3], outNod, method="nearest")
    return interpo


def main():
    ver, tri = ss.openMesh(os.sep.join(['.'+ss.dir_mesh, ss.mesh_name]))

    cen, s_sta_cells = ss.identifyStations(ver, tri[:,1:], np.asarray([]), np.asarray([]))

    df_sinfo = pd.read_csv(os.sep.join(['.'+ss.dir_data, ss.sta_name]), sep=' ')
    df_sinfo = df_sinfo.loc[:,['Stations', 'x_coord', 'y_coord']]
    df_sinfo['y_coord'] = df_sinfo['y_coord'] - 1e+7

    df_ginfo = pd.read_csv(os.sep.join(['.'+ss.dir_data, ss.coor_name]), sep=' ')

    df_sdata = ss.openSStationsData(os.sep.join(['.'+ss.dir_data, ss.sda_name]))
    df_sdata['Date'] = pd.to_datetime(df_sdata['Date'], format='%d/%m/%Y-%H:%M:%S')

    df_gdata = ss.openGStationsData(os.sep.join(['.'+ss.dir_data, ss.data_name]))


    print(df_gdata)
    points = np.asarray([[225402, 3462300],
                         [210554, 3444500],
                         [229523, 3431400]])
    values = np.asarray([[20, 1, 0, 0],
                         [20, 0, 0.1, 0],
                         [20, 0, 0, 0.1]])

    inte = InitialCondition(points, values, cen)

    plotFlag = True
    saveFlag = False
    if plotFlag:
        fig = plt.figure()

        ax = fig.add_subplot(1, 1, 1, projection="3d")

        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'$y$')
        ax.set_zlabel(r'$z$')

        ax.plot(cen[:,0], cen[:,1], inte[:,2],"^")

        plt.show()

    if saveFlag:
        np.savetxt(dirname + "biological_taihu_nu.ini",inte)

main()
