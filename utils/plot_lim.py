import os
import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

ref = dt.datetime(2001,5,1,0,0,0)

river_to_node = {
    "Dapu_River" : 2,
    "Sanliqiao" : 3,
    "Cao&huangyan" : 4,
    "Wulihuzha" : 5,
    "Wangyu_River" : 6,
    "Dushanzha" : 5,
    "Huguang_Canal" : 7,
    "Gulougang" : 8,
    "Huazhuang&Zhonghua" : 9,
    "Chendonggang" : 10,
    "Changxinggang" : 11,
    "Dongtiaoxi" : 12,
    "Xitiaoxi" : 13,
    "Xinyunhe_Canal" : 14,
    "Xujiang_River" : 15,
    "Wujingang" : 16,
    "Taipu_River" : 14
}

def openRiver(path_file_name):
    df_river = pd.read_csv(path_file_name, sep=' ', skiprows=2, header=None)

    df_river = df_river.rename(columns={0:'Name_river', 1: 'Date', 2:'flux' ,3:'temp',4:'salt',5:'CODMn',6:'NH4',7:'NO2',8:'NO3',9:'TN',10:'TP',11:'PO4'})

    df_river['Date'] = pd.to_datetime(df_river['Date'], format='%d/%m/%Y-%H:%M:%S')
    return df_river

def plotRivers(df, df_lim):
    n_row = 5
    n_col = 4

    fig, ax = plt.subplots(n_row, n_col)
    fig.suptitle('Rivers Flux Data')

    l_riv_nam = list(df['Name_river'].unique())
    n_rivers = len(l_riv_nam)
    print(n_rivers)
    for i in range(n_rivers):
        row = i // n_col
        col = i % n_col

        # Data
        aax = ax[row][col]
        aax.set_title('River %s %i'%(l_riv_nam[i],i))
        #aax.plot(df.loc[df['Name_river']==l_riv_nam[i],'Date'], df.loc[df['Name_river']==l_riv_nam[i],'flux'], '-')
        #aax.plot(df.loc[df['Name_river']==l_riv_nam[i],'Date'], 0*df.loc[df['Name_river']==l_riv_nam[i],'flux'], 'r--')
        aax.set_xticks([])

        if l_riv_nam[i] in river_to_node.keys():
            aax.plot(df_lim[0], df_lim[river_to_node[l_riv_nam[i]]])

        aax.plot(df_lim[0], [0]*df_lim[0].size, '--')


    ax[4][2].remove()
    ax[4][3].remove()
    plt.margins(x=0, y=0)

dir_data = './data'
rive_name = 'rivers_LakeTaihu_dat.txt'

dir_name = '/home/jkahncas/Development/calibration/'
lim = 'test/Calibration_small/input/Taihu.lim'

columns = ['time','bc']
for i in range(1,18):
    columns.append(i)

df_lim = pd.read_csv(os.sep.join([dir_name, lim]),header=None, sep=' ', skiprows=8)

for i in range(df_lim[0].size):
    df_lim.loc[i,0] = ref + dt.timedelta(seconds=df_lim.loc[i,0])


df_rive = openRiver(os.sep.join(['../'+dir_data, rive_name]))
print(df_rive)
plotRivers(df_rive, df_lim)

plt.show()
