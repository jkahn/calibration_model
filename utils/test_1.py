#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.4.0 with dump python functionality
###
import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/user/jkahncas/home')

import os
import sys
sys.path.insert(0, r'/home/jkahncas/Development/calibration')


ver = []
with open('/home/jkahncas/Development/calibration/mesh/taihu_small/Vertices.txt') as f:
    lines = f.readlines()
    for line in lines:
        values = line.split('\n')[0].split(' ')
        if len(values) == 3:
            values = [float (value) for value in values]
            ver.append(values)


tri = []
with open('/home/jkahncas/Development/calibration/mesh/taihu_small/Triangles.txt') as f:
    lines = f.readlines()
    for line in lines:
        values = line.split('\n')[0].split(' ')
        if len(values) == 4:
            values = [int(value)-1 for value in values]
            tri.append(values)


lados = []
for t in tri:
    a = [t[1],t[2]]
    b = [t[2],t[3]]
    c = [t[3],t[1]]
    lados.append(a)
    lados.append(b)
    lados.append(c)

index = [0] * len(lados)
for i in range(len(lados)):
    for l in lados:
        if lados[i] == l or lados[i] == l[::-1]:
            index[i] += 1

boundary = []

for i in range(len(lados)):
    if index[i] == 1:
        boundary.append(lados[i])



###
### GEOM component
###
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

geompy = geomBuilder.New()

#Line_1 = geompy.MakeLineTwoPnt(Vertex_2, Vertex_1)

#Adding the nodes
vertices = []
for i in range(len(ver)):
    vertices.append(geompy.MakeVertex(ver[i][0], ver[i][1], 0))
    geompy.addToStudy(vertices[i] , 'Vertex_%i'%(i+1))

#Adding the edges
lineas = []
for b in boundary:
    lineas.append(geompy.MakeLineTwoPnt(vertices[b[0]], vertices[b[1]]))
#geompy.addToStudy( Vertex_1, 'Vertex_1' )
#geompy.addToStudy( Vertex_2, 'Vertex_2' )

for i in range(len(lineas)):
    geompy.addToStudy( lineas[i], 'Line_%i'%(i+1))

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
