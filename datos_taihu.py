import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import pandas as pd

dirname = ""
if sys.platform == "win32":
    dirname += ".\\sw2d\\Data_Taihu"
elif sys.platform == "linux":
    dirname += "./sw2d/Data_Taihu"

sta1_filename = "obs_data_LakeTaihu_dat.txt"
sta2_filename = "heat_flux_LakeTaihu_dat.txt"
rive_filename = "rivers_LakeTaihu_dat.txt"

#lon/lat
sta_filename = "longlat_water_obs_station_LakeTaihu.txt"
met_filename = "longlat_meteo_LakeTaihu.txt"
riv_filename = "longlat_rivers_loc_LakeTaihu.txt"

reference = dt.datetime(2001,5,1,0,0,0)

node_to_river = {
    3741 : "Dapu_River",
    2297 : "Sanliqiao",
    535 : "Cao&huangyan",
    2152 : "Wulihuzha",
    3451 : "Wangyu_River",
    2086 : "Dushanzha",
    3160 : "Huguang_Canal",
    2683 : "Gulougang",
    3178 : "Huazhuang&Zhonghua",
    105 : "Chendonggang",
    334 : "Changxinggang",
    1760 : "Dongtiaoxi",
    1239 : "Xitiaoxi",
    3621 : "Xinyunhe_Canal",
    3619 : "Xujiang_River",
    1302 : "Wujingang",
    3630 : "Taipu_River"
}

node_to_ad = {
    3741 : 3740,
    2297 : 2224,
    535  : 564,
    2152 : 2205,
    3451 : 3442,
    2086 : 2152,
    3160 : 3121,
    2683 : 2616,
    3178 : 3224,
    105  : 122,
    334  : 322,
    1760 : 1685,
    1239 : 1194,
    3621 : 3599,
    3619 : 3640,
    1302 : 1362,
    3630 : 3621,
}

river_to_node = {
    "Dapu_River" : 2,
    "Sanliqiao" : 3,
    "Cao&huangyan" : 4,
    "Wulihuzha" : 5,
    "Wangyu_River" : 6,
    "Dushanzha" : 7,
    "Huguang_Canal" : 8,
    "Gulougang" : 9,
    "Huazhuang&Zhonghua" : 10,
    "Chendonggang" : 11,
    "Changxinggang" : 12,
    "Dongtiaoxi" : 13,
    "Xitiaoxi" : 14,
    "Xinyunhe_Canal" : 15,
    "Xujiang_River" : 16,
    "Wujingang" : 17,
    "Taipu_River" : 18
}

River_names = {
    1: "Dapu_River",
    2: "Sanliqiao",
    3: "Cao&huangyan",
    4: "Wulihuzha",
    5: "Wangyu_River",
    6: "Dushanzha",
    7: "Huguang_Canal",
    8: "Gulougang",
    9: "Huazhuang&Zhonghua",
    10: "Chendonggang",
    11: "Changxinggang",
    12: "Dongtiaoxi",
    13: "Xitiaoxi",
    14: "Xinyunhe_Canal",
    15: "Xujiang_River",
    16: "Wujingang",
    17: "Taipu_River"
}

def importarDatos(filename):
    data = pd.read_csv(filename,sep=' ')
    return data

def plotTemperature():
    # Importe de datos
    df1 = importarDatos(dirname + os.sep + sta1_filename)
    df2 = importarDatos(dirname + os.sep + sta2_filename)
    df3 = importarDatos(dirname + os.sep + rive_filename)

    df1["Date"] = pd.to_datetime(df1["Date"],dayfirst=True,exact=True)
    df2["Date"] = pd.to_datetime(df2["Date"],dayfirst=True,exact=True)
    df3["Date"] = pd.to_datetime(df3["Date"],dayfirst=True,exact=True)


    #print(df1.columns) #-> "temp"
    #print(df2.columns) #-> "air"
    #print(df3.columns) #-> "temp"

    # Estacion
    station_id = 6

    # Tiempo
    date_from = pd.Timestamp(dt.date(2001,1,1))
    date_to = pd.Timestamp(dt.date(2002,12,31))

    station_water = False
    station_air = False
    river_water = False
    comparation_station = True

    

    # Station: Water Temperature
    if station_water:
        plt.figure("Water Temperature of Stations")
        plt.title("Water Temperature of Stations")
        for station_id in range(1,11):
            estacion_1 = df1["Stations_id"] == station_id
            Fecha_inicio_1 = df1["Date"] >= date_from
            Fecha_final_1 = df1["Date"] <= date_to
            isnan_1 =  df1["temp"].notna()

            plt.plot_date(df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["Date"]],
                        df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["temp"]],
                        "-",
                        label = "Station id " + str(station_id) + " (°C)")
        for station_id in range(11,15):
            estacion_1 = df1["Stations_id"] == station_id
            Fecha_inicio_1 = df1["Date"] >= date_from
            Fecha_final_1 = df1["Date"] <= date_to
            isnan_1 =  df1["temp"].notna()

            plt.plot_date(df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["Date"]],
                        df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["temp"]],
                        "--",
                        label = "Station id " + str(station_id) + " (°C)")


    # Station: Air Temperature
    if station_air:
        plt.figure("Air Temperature of Stations")
        plt.title("Air Temperature of Stations")
        air_list = range(1,9)#[4,6,7,8]
        for station_id in air_list:
            estacion_2 = df2["Stations_id"] == station_id
            Fecha_inicio_2 = df2["Date"] >= date_from
            Fecha_final_2 = df2["Date"] <= date_to
            isnan_2 =  df2["air"].notna()

            plt.plot_date(df2.loc[estacion_2 & Fecha_inicio_2 & Fecha_final_2 & isnan_2, ["Date"]],
                        df2.loc[estacion_2 & Fecha_inicio_2 & Fecha_final_2 & isnan_2, ["air"]],
                        "-",
                        label = "Station id " + str(station_id)  + " (°C)")


    if river_water:
        # River: Water Temperature
        plt.figure("Water Temperature of Rivers")
        plt.title("Water Temperature of Rivers")
        for i in range(1,18):
            plt.title("Water Temperature of Rivers")
            river = df3["Name_river"] == River_names[i]
            Fecha_inicio_3 = df3["Date"] >= date_from
            Fecha_final_3 = df3["Date"] <= date_to
            isnan_3 =  df3["temp"].notna()

            plt.plot_date(df3.loc[river & Fecha_inicio_3 & Fecha_final_3 & isnan_3, ["Date"]],
                        df3.loc[river & Fecha_inicio_3 & Fecha_final_3 & isnan_3, ["temp"]],
                        "-",
                        label = "River " + River_names[i] + " (°C)")

    if comparation_station:
        plt.figure("Average Air/Water Temperature of Stations")
        plt.title("Average Air/Water Temperature of Stations")
        water_data = pd.DataFrame(columns = ["Date", "temp"])
        water_data["Date"] = pd.date_range(pd.Timestamp(dt.datetime(2001,1,1,12,0,0)), periods=24, freq='2SM')
        water_data["temp"] = 0
        
        water_list = range(1,11)
        for station_id in water_list:
            estacion_1 = df1["Stations_id"] == station_id
            Fecha_inicio_1 = df1["Date"] >= date_from
            Fecha_final_1 = df1["Date"] <= date_to
            isnan_1 =  df1["temp"].notna()
            #plt.plot_date(df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["Date"]], df1.loc[estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1, ["temp"]], "--", label = "Station id " + str(station_id)  + " (°C)")
            water_data["temp"] += df1["temp"][estacion_1 & Fecha_inicio_1 & Fecha_final_1 & isnan_1].values
        water_data["temp"] /= len(water_list)
        plt.plot_date(water_data["Date"], water_data["temp"], "b-", label = "Water T (°C)")

        air_data = pd.DataFrame(columns = ["Date", "air"])
        air_data["Date"] = df2["Date"][(df2["Stations_id"] == 1) & (df2["Date"] >= date_from) & (df2["Date"] <= date_to) & df2["air"].notna()]
        air_data["air"] = 0

        air_list = [4,6,7,8]
        #air_list = range(1,9)
        for station_id in air_list:
            estacion_2 = df2["Stations_id"] == station_id
            Fecha_inicio_2 = df2["Date"] >= date_from
            Fecha_final_2 = df2["Date"] <= date_to
            isnan_2 =  df2["air"].notna()
            #plt.plot_date(df2.loc[estacion_2 & Fecha_inicio_2 & Fecha_final_2 & isnan_2, ["Date"]], df2.loc[estacion_2 & Fecha_inicio_2 & Fecha_final_2 & isnan_2, ["air"]], "--", label = "Station id " + str(station_id)  + " (°C)")
            air_data["air"] += df2["air"][estacion_2 & Fecha_inicio_2 & Fecha_final_2 & isnan_2].values
        air_data["air"] /= len(air_list)
        plt.plot_date(air_data["Date"], air_data["air"], "r-", label = "Air T (°C)")

    plt.xlabel("Date")
    plt.ylabel("Temperature")
    plt.legend()
    plt.show()

def plotStations():
    # import mesh with pandas

    # lon/lat
    df_sta = importarDatos(dirname + os.sep + sta_filename)
    df_met = importarDatos(dirname + os.sep + met_filename)
    df_riv = importarDatos(dirname + os.sep + riv_filename)

    station_phyto = False

    fig = plt.figure()
    # Water Stations
    plt.plot(df_sta["longitude"], df_sta["latitude"], 'r^',markersize=20)

    # Air Stations
    air_list = range(8)
    #air_list = [3,5,6,7]
    for station_id in air_list:
        plt.plot(df_met["longitude"][station_id], df_met["latitude"][station_id],"b*",markersize=20)

    # Rivers
    plt.plot(df_riv["longitude"], df_riv["latitude"],"gd",markersize=20)
    plt.plot(df_riv["longitude"][8], df_riv["latitude"][8],"gd",markersize=50)
    

    import mplleaflet
    mplleaflet.show()

def plotMesh():
    ver_file = "LakeTaihu_cor.dat"
    tri_file = "LakeTaihu_elements.txt"
    df_ver = pd.read_csv(dirname + os.sep + ver_file, sep='\s+', header=None)
    df_ver.columns = ["x", "y", "z"]
    df_tri = pd.read_csv(dirname + os.sep + tri_file, sep='\s+', header=None,dtype=int)
    df_tri.columns = ["t1", "t2", "t3"]

    plt.figure()

    plt.plot(df_ver["x"], df_ver["y"],'b^')
    for river in node_to_river.keys():
        
        plt.plot(df_ver["x"][river-1], df_ver["y"][river-1],'r^')
        plt.text(df_ver["x"][river-1], df_ver["y"][river-1],node_to_river[river])
        node_ad = node_to_ad[river]
        plt.plot(df_ver["x"][node_ad-1], df_ver["y"][node_ad-1],'g^')

    plt.show()
    #import mplleaflet
    #mplleaflet.show()
    

def main():
    #plotTemperature()
    #plotStations()
    plotMesh()

main()