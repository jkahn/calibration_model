import os
import re
import vtk
import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.interpolate as inter
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

patron = re.compile(r'\s{1,}')

dir_name = '.'

# Data Files
dir_data = './data'
sta_name = 'xy_water_obs_station_LakeTaihu.txt'
sda_name = 'obs_data_LakeTaihu_dat.txt'

data_name = 'Water_level_data.txt'
coor_name = 'Water_level_stations.txt'

rive_name = 'rivers_LakeTaihu_dat.txt'
wind_name = 'wind_LakeTaihu_dat.txt'

# Simulation Test
dir_simu = './test/Calibration_medium'
#dir_simu = './test/Calibration'
#dir_simu = './test/Calibration'
#dir_simu = '/media/joseph/LaCie/Test/Calibration_small'

# Simulation Folder
iput = 'input'
misc = 'misc'
log = 'log'
oput = 'output'

#time: 31536000
reference = dt.datetime(2001,5,1,0,0,0)

rive_dicc = {
    'Dapu_River'        : 2,
    'Sanliqiao'         : 3,
    'Cao&huangyan'      : 4,
    'Wulihuzha'         : 5,
    'Wangyu_River'      : 6,
    'Dushanzha'         : 7,
    'Huguang_Canal'     : 8,
    'Gulougang'         : 9,
    'Huazhuang&Zhonghua': 10,
    'Chendonggang'      : 11,
    'Changxinggang'     : 12,
    'Dongtiaoxi'        : 13,
    'Xitiaoxi'          : 14,
    'Xinyunhe_Canal'    : 15,
    'Xujiang_River'     : 16,
    'Wujingang'         : 17,
    'Taipu_River'       : 18
}

River_names = {
    1   : "Dapu_River",
    2   : "Sanliqiao",
    3   : "Cao&huangyan",
    4   : "Wulihuzha",
    5   : "Wangyu_River",
    6   : "Dushanzha",
    7   : "Huguang_Canal",
    8   : "Gulougang",
    9   : "Huazhuang&Zhonghua",
    10  : "Chendonggang",
    11  : "Changxinggang",
    12  : "Dongtiaoxi",
    13  : "Xitiaoxi",
    14  : "Xinyunhe_Canal",
    15  : "Xujiang_River",
    16  : "Wujingang",
    17  : "Taipu_River"
}


node_to_river = {
    3741 : ["Dapu_River",165],
    2297 : ["Sanliqiao",119],
    535 : ["Cao&huangyan",231],
    2152 : ["Wulihuzha",248],
    3451 : ["Wangyu_River",386],
    2086 : ["Dushanzha",161],
    3160 : ["Huguang_Canal",92.6],
    2683 : ["Gulougang", 58.8],
    3178 : ["Huazhuang&Zhonghua",150],
    105 : ["Chendonggang",118],
    334 : ["Changxinggang",170],
    1760 : ["Dongtiaoxi",107],
    1239 : ["Xitiaoxi",308],
    3621 : ["Xinyunhe_Canal",69.6],
    3619 : ["Xujiang_River",169],
    1302 : ["Wujingang",205],
    3630 : ["Taipu_River",557]
}

river_to_node = {
    "Dapu_River" : 2,
    "Sanliqiao" : 3,
    "Cao&huangyan" : 4,
    "Wulihuzha" : 5,
    "Wangyu_River" : 6,
    "Dushanzha" : 5,
    "Huguang_Canal" : 7,
    "Gulougang" : 8,
    "Huazhuang&Zhonghua" : 9,
    "Chendonggang" : 10,
    "Changxinggang" : 11,
    "Dongtiaoxi" : 12,
    "Xitiaoxi" : 13,
    "Xinyunhe_Canal" : 14,
    "Xujiang_River" : 15,
    "Wujingang" : 16,
    "Taipu_River" : 14
}


# Mesh
dir_mesh = './mesh'
if '_small' in dir_simu:
    mesh_name = 'taihu_small'
elif '_medium' in dir_simu:
    mesh_name = 'taihu_medium'
else:
    mesh_name = 'taihu'

white_list = ['water_depth', 'surface_elevation', 'x_unit_discharge', 'y_unit_discharge']
white_list_bio = ['heat', 'cocnentration1', 'concentration2', 'concentration3']

def openVtkFile(path_vtp_file):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(path_vtp_file)
    reader.Update()
    polydata = reader.GetOutput()
    cell_data = polydata.GetCellData()
    ncells = polydata.GetNumberOfCells()
    ncomps = cell_data.GetNumberOfArrays()
    df_ini = pd.DataFrame()
    for ite in range(ncomps):
        component_name = cell_data.GetArrayName(ite)
        if (component_name in white_list) or ('concentration' in component_name) or ('heat' in component_name):
            component_data = cell_data.GetArray(component_name).GetTuple1
            aux = np.zeros(ncells)
            for i in range(ncells):
                aux[i] = component_data(i)
            df_ini[component_name] = aux
    return df_ini


# open with csv file
def openCSV(path_file_name):
    df_end = pd.read_csv(path_file_name, sep='\s+', index_col=None, skiprows=0,escapechar='#')
    return df_end


def openMesh(path_file_name):
    print('----- Mesh -----')
    print('> mesh path: %s'%path_file_name)
    ver = np.loadtxt(os.sep.join([path_file_name,'Vertices.txt']))
    tri = np.loadtxt(os.sep.join([path_file_name,'Triangles.txt']), dtype=int) - 1
    print('> mesh was opened\n')
    return ver, tri


def openStations(path_folder_name, s_file_name, g_file_name):
    print('----- Stations Info -----')
    path_s_file_name = os.sep.join([path_folder_name, s_file_name])
    path_g_file_name = os.sep.join([path_folder_name, g_file_name])

    print('> S stations info: %s'%path_s_file_name)
    df_s = pd.read_csv(path_s_file_name, sep=' ')
    df_s = df_s[df_s['Stations'] <= 10][['Stations', 'x_coord', 'y_coord']]
    df_s['Stations'] = pd.to_numeric(df_s['Stations'])
    df_s['x_coord'] = pd.to_numeric(df_s['x_coord'])
    df_s['y_coord'] = pd.to_numeric(df_s['y_coord'])
    df_s['y_coord'] = df_s['y_coord'] - 1e7

    print('> G stations info: %s'%path_g_file_name)
    df_g = pd.read_csv(path_g_file_name, sep=' ')

    print('> Stations info was opened\n')
    return df_s, df_g


def openSStationsData(path_file_name):
    print('----- S Stations Data -----')
    print('> stations data: %s'%path_file_name)
    df = pd.read_csv(path_file_name, sep=' ')
    df = df[df['Stations_id'] <= 10][['Stations_id', 'Date', 'depth', 'temp']]
    df['Date'] = pd.to_datetime(df['Date'], format='%d/%m/%Y-%H:%M:%S')
    df['Stations_id'] = pd.to_numeric(df['Stations_id'])
    df['depth'] = pd.to_numeric(df['depth'])
    df['temp'] = pd.to_numeric(df['temp'])
    print('> stations data was opened\n')
    return df

def openGStationsData(path_file_name):
    print('----- G Stations Data -----')
    print('> s stations data: %s'%path_file_name)
    df_all = pd.read_csv(path_file_name, sep='\t')
    df = pd.DataFrame(columns = ['Stations_id', 'Date', 'water_depth'], index = [])

    g_sta_name = list(df_all.keys())
    n_g_sta = len(g_sta_name)
    for i in range(1,n_g_sta):
        a_df = df_all.loc[:, ['date', g_sta_name[i]]]
        a_df = a_df.rename(columns={'date':'Date', g_sta_name[i]:'water_depth'})
        a_df['Stations_id'] = i
        a_df = a_df.loc[~np.isnan(a_df['water_depth']), :]
        df = df.append(a_df, ignore_index=True, sort=True)

    print('> g stations data was opened\n')
    return df

def openNewData(data_file_name, coor_file_name):
    data = pd.read_csv(data_file_name, sep='\s+', engine='python')
    lonlat = pd.read_csv(coor_file_name, sep='\s+', engine='python')
    data['date'] = pd.to_datetime(data['date'], format='%d/%m/%Y')
    data = deleteNanVales(data, lonlat)
    return data, lonlat


def deleteNanVales(df, info):
    for station in info['Station_ID'].values:
        if station == 'Wangting':
            df[station] = df[station].interpolate(method='nearest')
            df.loc[155:,station] = df.loc[154,station]
        elif station == 'Xishan':
            df[station] = df[station].interpolate(method='nearest')
        elif station == 'Xiaomeikou':
            df[station] = df[station].interpolate(method='nearest')
            df.loc[:6,station] = df.loc[6,station]
        else:
            df[station] = df[station].interpolate()
    return df


def generateDFSimu(stations_cells, path, reference):
    print('\n----- Open Simulation Results -----')
    print('> simulation results: %s'%path)
    list_dir = os.listdir(path)
    list_dir = [name for name in list_dir if ('ResultHydro_' in name) and ('.vtp' in name)]
    df = pd.DataFrame(columns = ['Stations_id', 'Date'] + white_list, index = [])
    df_aux = pd.DataFrame(columns = ['Stations_id', 'Date'] + white_list, index = [0])
    for file_vtp in list_dir:
        df_data = openVtkFile(os.sep.join([path,file_vtp]))
        if 'layer' in file_vtp:
            split_time = float(file_vtp.split('ResultHydro_frame_')[1].split('_layer_0.vtp')[0]) * 3600
        else:
            split_time = float(file_vtp.split('ResultHydro_')[1].split('.vtp')[0])
        time = reference + dt.timedelta(seconds=split_time)
        for ite in range(len(stations_cells)):
            if stations_cells[ite] != 'out':
                for c_name in white_list:
                    df_aux.loc[0,c_name] = df_data.loc[stations_cells[ite], c_name]
                df_aux.loc[0,'Stations_id'] = ite + 1
                df_aux.loc[0,'Date'] = time
                df = df.append(df_aux, ignore_index=True)
    print('> simulation results opened')
    return df

#Agregar la impletentacion para generara el dataframe con los archivos csv
def generateDFBioSimu(stations_cells, path, reference):
    df = 0
    return df

def generateCenter(ver, tri):
    ncells = tri.shape[0]
    cen = np.zeros([ncells, 2])
    for cell in range(ncells):
        for node in tri[cell,1:]:
            cen[cell,0] += ver[node,0]
            cen[cell,1] += ver[node,1]
    cen = cen / 3.
    return np.asarray(cen)

def identifyStations(ver, tri, x_sta, y_sta):
    print('----- Identify Stations on Mesh -----')
    nvers = ver.shape[0]
    ncells = tri.shape[0]
    ncomps = x_sta.shape[0]
    cen = np.zeros([ncells, 2])
    for cell in range(ncells):
        for node in tri[cell]:
            cen[cell,0] += ver[node,0]
            cen[cell,1] += ver[node,1]
        cen[cell] = cen[cell] / 3
    stations_cells = []
    for sta in range(ncomps):
        for cell in range(ncells):
            status = True
            for i in range(3):
                if i<2:
                    a = tri[cell,i]
                    b = tri[cell,i+1]
                else:
                    a = tri[cell,i]
                    b = tri[cell,0]
                direct_1 = ver[b,:2] - ver[a,:2]
                direct_2 = cen[cell] - ver[a,:2]
                direct_3 = np.asarray([x_sta[sta], y_sta[sta]]) - ver[a,:2]
                normal = direct_2 - direct_1 * sum(direct_1 * direct_2) / sum(direct_1 * direct_1)
                val = sum(normal*direct_3)
                if val < 0:
                    status = False
            if status:
                stations_cells.append(cell)
                break
        if not status:
            stations_cells.append('out')
            print('> Stations out the mesh: %i, (x,y): %.2f %.2f'%(sta, x_sta[sta], y_sta[sta]))
    return cen, stations_cells


def openRiver(path_file_name):
    df_river = pd.read_csv(path_file_name, sep=' ', skiprows=2, header=None)

    df_river = df_river.rename(columns={0:'Name_river', 1: 'Date', 2:'flux' ,3:'temp',4:'salt',5:'CODMn',6:'NH4',7:'NO2',8:'NO3',9:'TN',10:'TP',11:'PO4'})

    df_river['Date'] = pd.to_datetime(df_river['Date'], format='%d/%m/%Y-%H:%M:%S')
    return df_river

def plotRivers(df):
    n_row = 5
    n_col = 4

    fig, ax = plt.subplots(n_row, n_col)
    fig.suptitle('Rivers Flux Data')

    l_riv_nam = list(df['Name_river'].unique())
    n_rivers = len(l_riv_nam)

    for i in range(n_rivers):
        row = i // n_col
        col = i % n_col

        # Data
        aax = ax[row][col]
        aax.set_title('River %s %i'%(l_riv_nam[i],i))
        aax.plot(df.loc[df['Name_river']==l_riv_nam[i],'Date'], df.loc[df['Name_river']==l_riv_nam[i],'flux'], '-')
        aax.plot(df.loc[df['Name_river']==l_riv_nam[i],'Date'], 0*df.loc[df['Name_river']==l_riv_nam[i],'flux'], 'r--')
        aax.set_xticks([])

    ax[4][2].remove()
    ax[4][3].remove()
    plt.margins(x=0, y=0)


def openWind(path_file_name):
    df_wind = pd.read_csv(path_file_name, sep=' ', skiprows=2)
    df_wind['x_velocity'] = - df_wind['velocity'] * np.sin(df_wind['theta'] * np.pi / 180.)
    df_wind['y_velocity'] = - df_wind['velocity'] * np.cos(df_wind['theta'] * np.pi / 180.)
    return df_wind

def plotWind(df):
    x_wind = df['x_velocity']
    y_wind = df['y_velocity']

    fig, ax = plt.subplots(1,1)
    color_array = np.sqrt(((x_wind)/2)**2 + ((y_wind)/2)**2)
    v_max = np.max(color_array)
    ini,fin,ste = [None,None,2]
    fig.suptitle('Wind Data Velocity')
    ax.quiver(df['time'][ini:fin:ste],0, x_wind[ini:fin:ste]/v_max, y_wind[ini:fin:ste]/v_max, color_array, alpha=0.8)

def plotStationData(id_stations, sta_cell, df_stations, df_simu):
    #fig = plt.figure('Station_Data_%i'%id_stations)
    fig, ax = plt.subplots()
    ax.set_title('Station Data %i'%id_stations)
    ax.plot(df_stations['Date'], df_stations['depth'], 'o-', label = 'Sta %i, depth '%id_stations)
    ax.plot(df_simu['Date'], df_simu[white_list[0]], 'o-', label = 'Sta %i,simu depth '%id_stations)
    ax.set_xlabel('Date')
    ax.set_ylabel('Depth')
    ax.set_ylim([0,6])
    ax.legend()

    #fig = plt.figure('Station_Simu_%i:'%id_stations)
    fig1, ax2 = plt.subplots()
    ax2.set_title('Station Data %i'%id_stations)
    ax2.plot(df_simu['Date'], df_simu[white_list[0]] * (df_simu[white_list[2]]**2 + df_simu[white_list[3]]**2)**0.5, 'o-', label = 'Sta %i,simu velocity '%id_stations)
    ax2.set_xlabel('Date')
    ax2.set_ylabel('velocity')
    ax2.legend()
    #plt.show()


def plotAllStations(S_id, df_data, df_simu):#, df_simu_2):
    index = df_data['Stations_id'].values
    index = np.unique(index)

    n_dataSta = len(index)
    n_simuSta = sum([1 if ele != 'out' else 0 for ele in S_id])

    #fig = plt.figure('Stations_Data')
    n_row = 3
    n_col = 3

    fig, ax = plt.subplots(n_row, n_col)
    fig.suptitle('Calibration Results')


    for i in range(n_simuSta):
        row = i // n_row
        col = i % n_col
        # Data

        aax = ax[row][col]
        aax.set_title('S%i'%(i+2))
        aax.plot(df_data.loc[df_data['Stations_id']==i+2,'Date'], df_data.loc[df_data['Stations_id']==i+2,'depth'], 'o-')
        # Simu
        aax.plot(df_simu.loc[df_simu['Stations_id'] == i+2,'Date'], df_simu.loc[ df_simu['Stations_id'] == i+2, white_list[0]], 'o-')
        #aax.plot(df_simu_2.loc[df_simu_2['Stations_id'] == i+2,'Date'], df_simu_2.loc[ df_simu_2['Stations_id'] == i+2, white_list[0]], 'o-')
        aax.set_ylim([0,4])

    ax[2][2].remove()
    #aax.#ax.legend()


def plotAllStationsData(df_stations):
    index = df_stations['Stations_id'].values
    index = np.unique(index)
    #fig = plt.figure('Stations_Data')
    fig3, ax3 = plt.subplots()
    ax3.set_title('Stations Data')
    for ite in index:
        df_aux = df_stations[df_stations['Stations_id'] == ite]
        ax3.plot(df_aux['Date'], df_aux['depth'], 'o-', label = 'Sta %i, depth '%ite)
    ax3.legend()


def plotAllStationsSimu(df_stations):
    index = df_stations['Stations_id'].values
    index = np.unique(index)
    #fig = plt.figure('Stations_Data')
    fig3, ax3 = plt.subplots()
    ax3.set_title('Stations Data')
    for ite in index:
        df_aux = df_stations[df_stations['Stations_id'] == ite]
        ax3.plot(df_aux['Date'], df_aux['water_depth'], 'o-', label = 'Sta %i, depth '%ite)
    ax3.legend()


def plotConcentration(df_simu):
    fig, ax = plt.subplots()

    ax = fig.add_subplot(311)
    ax.plot(df_simu['Date'], df_simu['concentration1'], 'gx', label='phy')
    ax = fig.add_subplot(312)
    ax.plot(df_simu['Date'], df_simu['concentration2'], 'rx', label='ip')
    ax = fig.add_subplot(313)
    ax.plot(df_simu['Date'], df_simu['concentration3'], 'bx', label='op')


def main(plotflag=False, id_sta = 8):
    # open mesh
    ver, tri = openMesh(os.sep.join(['../'+dir_mesh, mesh_name]))

    # open stations coordinate
    s_sta, g_sta = openStations('../'+dir_data, sta_name, coor_name)
    xs = s_sta['x_coord'].values
    ys = s_sta['y_coord'].values
    xg = g_sta['x'].values
    yg = g_sta['y'].values

    # identify stations on the mesh
    cen, s_sta_cells = identifyStations(ver, tri[:,1:], xs, ys)
    cen, g_sta_cells = identifyStations(ver, tri[:,1:], xg, yg)

    # open stations data
    df_sdata = openSStationsData(os.sep.join(['../'+dir_data, sda_name]))
    df_gdata = openGStationsData(os.sep.join(['../'+dir_data, data_name]))

    # rivers flux and wind
    df_rive = openRiver(os.sep.join(['../'+dir_data, rive_name]))
    df_wind = openWind(os.sep.join(['../'+dir_data, wind_name]))

    # open simulation results
    #reference = dt.datetime(2001,5,1,0,0,0)
    df_simu = generateDFSimu(s_sta_cells, os.sep.join(['../'+dir_simu, misc]), reference)
    df_simu = df_simu.sort_values(by=['Date'])

    #reference = dt.datetime(2001,11,1,0,0,0)
    #df_simu_2 = generateDFSimu(s_sta_cells, os.sep.join([dir_simu, misc]), reference)
    #df_simu_2 = df_simu_2.sort_values(by=['Date'])

    if plotflag:
        # Plot one station results
        if s_sta_cells[id_sta - 1] != 'out':
            plotStationData(id_sta, s_sta_cells[id_sta - 1], df_sdata[df_sdata['Stations_id'] == id_sta],
            df_simu[df_simu['Stations_id'] == id_sta])

        # plot all the stations data
        plotAllStations(s_sta_cells,df_sdata,df_simu)#,df_simu_2)


        #plotAllStationsData(df_data)
        #plotAllStationsSimu(df_simu)

        # plot rivers flux
        plotRivers(df_rive)

        # plot wind velocity
        plotWind(df_wind)

        #plotConcentration(df_simu)

        # Plot
        #fig = plt.figure('Mesh')
        fig4, ax4 = plt.subplots()

        # plotmesh
        ax4.triplot(ver[:,0], ver[:,1], tri[:,1:])

        # plotstations
        for i in range(len(s_sta_cells)):
            if i != 0 and i != 9:
                ax4.plot(xs[i], ys[i], '^',label='%i'%i)
                ax4.plot(cen[s_sta_cells[i],0], cen[s_sta_cells[i],1], 'r^')
        ax4.legend()
        plt.show()
    #return stations_cells, df_data, df_simu




def main_bio(plotflag=False, id_sta = 8):
    # open mesh
    ver, tri = openMesh(os.sep.join(['../'+dir_mesh, mesh_name]))

    # open stations coordinate
    s_sta, g_sta = openStations('../'+dir_data, sta_name, coor_name)

    xs = s_sta['x_coord'].values
    ys = s_sta['y_coord'].values

    xg = g_sta['x'].values
    yg = g_sta['y'].values

    # identify stations on the mesh
    cen, s_sta_cells = identifyStations(ver, tri[:,1:], xs, ys)
    cen, g_sta_cells = identifyStations(ver, tri[:,1:], xg, yg)

    # open stations data
    df_sdata = openSStationsData(os.sep.join(['../'+dir_data, sda_name]))
    df_gdata = openGStationsData(os.sep.join(['../'+dir_data, data_name]))

    # rivers flux and wind
    df_rive = openRiver(os.sep.join(['../'+dir_data, rive_name]))
    df_wind = openWind(os.sep.join(['../'+dir_data, wind_name]))

    # open simulation results
    #reference = dt.datetime(2001,5,1,0,0,0)
    df_simu = generateDFSimu(s_sta_cells, os.sep.join(['../'+dir_simu, misc]), reference)
    df_simu = df_simu.sort_values(by=['Date'])

    #reference = dt.datetime(2001,11,1,0,0,0)
    #df_simu_2 = generateDFSimu(s_sta_cells, os.sep.join([dir_simu, misc]), reference)
    #df_simu_2 = df_simu_2.sort_values(by=['Date'])

    if plotflag:
        # Plot one station results
        if s_sta_cells[id_sta - 1] != 'out':
            plotStationData(id_sta, s_sta_cells[id_sta - 1], df_sdata[df_sdata['Stations_id'] == id_sta],
            df_simu[df_simu['Stations_id'] == id_sta])

        # plot all the stations data
        plotAllStations(s_sta_cells,df_sdata,df_simu)#,df_simu_2)


        #plotAllStationsData(df_data)
        #plotAllStationsSimu(df_simu)

        # plot rivers flux
        plotRivers(df_rive)

        # plot wind velocity
        plotWind(df_wind)

        #plotConcentration(df_simu)

        # Plot
        #fig = plt.figure('Mesh')
        fig4, ax4 = plt.subplots()

        # plotmesh
        ax4.triplot(ver[:,0], ver[:,1], tri[:,1:])

        # plotstations
        for i in range(len(s_sta_cells)):
            if i != 0 and i != 9:
                ax4.plot(xs[i], ys[i], '^',label='%i'%i)
                ax4.plot(cen[s_sta_cells[i],0], cen[s_sta_cells[i],1], 'r^')
        ax4.legend()
        plt.show()

#main(True)
