#https://docs.scipy.org/doc/scipy-0.18.1/reference/tutorial/optimize.html
#https://docs.scipy.org/doc/scipy-0.18.1/reference/optimize.html

import os
import re
import vtk

import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.optimize import minimize
from scipy.integrate import trapz

import src.show_stations as ss

patron = re.compile(r'\s+')

reference = dt.datetime(2001,5,1,0,0,0)

x_in  = 9.93866656e-01
x_out = 5.45077183e-01

ssw2d='/user/jkahncas/home/Development/sw2d/sw2d/build/bin/sw2dSolver'
#ssw2d='/home/joseph/Develop/sw2d/sw2d/build/bin/sw2dSolver'

test_in = os.sep.join([ss.dir_simu, 'input', 'Taihu.in'])
test_lim = os.sep.join([ss.dir_simu, 'input', 'Taihu.lim'])

def backupBC():
    print('----- Verifying the backup -----')

    status = False

    new_name = test_lim.replace('.lim', '_backup.lim')

    if not os.path.isfile(new_name):
        print('> There is not a backup')
        print('> Making a backup')
        if not os.system('cp %s %s'%(test_lim, new_name)):
            print('> Backup was created')
            status = True
    else :
        print('> There is a backup')
        status = True

    return status


def updateBC(pathfile, theta_in, theta_out):
    print('----- Updating file .lim -----')
    bck = pathfile.replace('.lim', '_backup.lim')
    print('> file .lim: %s'%pathfile)
    with open(bck, 'r') as f:
        print('> file .lim opened')
        lines = f.readlines()
        nbc = int(lines[2])
        for j in range(8,len(lines)):
            line = [float(val) for val in patron.split(lines[j])[:-1]]
            for i in range(2,nbc+1):
                if line[i] > 0:
                    line[i] = theta_in * line[i]
                elif line[i] < 0:
                    line[i] = theta_out * line[i]
            line = ' '.join([str(val) for val in line])  + '\n'
            lines[j] = line
        lines = ''.join(lines)
    with open(pathfile, 'w') as f:
        f.write(lines)
        print('> file .lim updated')


def meanValueData(df):
    for i in range(1,11):
        aux = df.loc[df['Stations_id'] == i, 'depth']
        df.loc[df['Stations_id'] == i,'depth_mean'] = df.loc[df['Stations_id'] == i, 'depth'] - aux.mean(axis=0)
    return df


def meanValueNewData(df):
    sname = df.columns.values[1:]
    for name in sname:
        aux = df[name]
        df[name] = df[name] - aux.mean(axis=0)
    return df

def IntegralFuncion(df_data, df_simu, stations_cells, new_data, new_simu, new_stations_cells):
    val_integral = 0

    for ite in range(1,1+len(stations_cells)):
        if stations_cells[ite-1] != 'out':
            x = df_data.loc[df_data['Stations_id'] == ite]
            y = df_simu.loc[df_simu['Stations_id'] == ite]

            ndata = y.shape[0]

            meanValue = y['water_depth'].mean(axis=0)

            columns = ['Date', 'depth', 'water_depth', 'reference', 'integral']
            df_integral = pd.DataFrame(columns=columns, index=list(range(ndata)))

            for i in range(ndata):
                t = y.iloc[i,1]

                sup = x.loc[(t < x['Date'])]
                inf = x.loc[(t >= x['Date'])]
                t_sup = sup.iloc[0,1]
                x_sup = sup.iloc[0,4]
                t_inf = inf.iloc[-1,1]
                x_inf = inf.iloc[-1,4]

                val = ((t_sup - t).total_seconds() * x_inf + (t - t_inf).total_seconds() * x_sup) / (t_sup - t_inf).total_seconds()

                df_integral.iloc[i,0] = y.iloc[i,1]
                df_integral.iloc[i,1] = val
                df_integral.iloc[i,2] = y.iloc[i,2]
                df_integral.iloc[i,3] = (y.iloc[i,1] - reference).total_seconds()
                df_integral.iloc[i,4] = (y.iloc[i,2] - val - meanValue) ** 2

            df_integral['Date'] = pd.to_datetime(df_integral['Date'], format='%d/%m/%Y-%H:%M:%S')
            df_integral['reference'] = pd.to_numeric(df_integral['reference'])
            df_integral['depth'] = pd.to_numeric(df_integral['depth'])
            df_integral['water_depth'] = pd.to_numeric(df_integral['water_depth'])
            df_integral['integral'] = pd.to_numeric(df_integral['integral'])

            df_integral = df_integral.sort_values(by=['Date'])

            aux = trapz(df_integral.loc[:,'integral'].values, df_integral.loc[:,'reference'].values)

            val_integral += aux / (df_integral.iloc[-1,0] - df_integral.iloc[0,0]).total_seconds()

    S_name = new_data.columns.values[1:]
    for ite in range(1,1+len(new_stations_cells)):
        if new_stations_cells[ite - 1] != 'out':
            x = new_data.loc[:,['date',S_name[ite - 1]]]
            y = new_simu.loc[new_simu['Stations_id'] == ite]

            #print(x, y)
            ndata = y.shape[0]

            meanValue = y['water_depth'].mean(axis=0)

            columns = ['Date', 'depth', 'water_depth', 'reference', 'integral']
            df_integral = pd.DataFrame(columns=columns, index=list(range(ndata)))

            for i in range(ndata):
                t = y.iloc[i,1]

                sup = x.loc[(t < x['date'])]
                inf = x.loc[(t >= x['date'])]

                t_sup = sup.iloc[0,0]
                x_sup = sup.iloc[0,1]
                t_inf = inf.iloc[-1,0]
                x_inf = inf.iloc[-1,1]

                val = ((t_sup - t).total_seconds() * x_inf + (t - t_inf).total_seconds() * x_sup) / (t_sup - t_inf).total_seconds()

                df_integral.iloc[i,0] = y.iloc[i,1]
                df_integral.iloc[i,1] = val
                df_integral.iloc[i,2] = y.iloc[i,2]
                df_integral.iloc[i,3] = (y.iloc[i,1] - reference).total_seconds()
                df_integral.iloc[i,4] = (y.iloc[i,2] - val - meanValue) ** 2

            df_integral['Date'] = pd.to_datetime(df_integral['Date'], format='%d/%m/%Y-%H:%M:%S')
            df_integral['reference'] = pd.to_numeric(df_integral['reference'])
            df_integral['depth'] = pd.to_numeric(df_integral['depth'])
            df_integral['water_depth'] = pd.to_numeric(df_integral['water_depth'])
            df_integral['integral'] = pd.to_numeric(df_integral['integral'])

            df_integral = df_integral.sort_values(by=['Date'])

            aux = trapz(df_integral.loc[:,'integral'].values, df_integral.loc[:,'reference'].values)

            val_integral += aux / (df_integral.iloc[-1,0] - df_integral.iloc[0,0]).total_seconds()
    return val_integral


def save_param(x):
    lines = ''' -------------------
    theta_in: %.8e
    theta_out: %.8e
    '''%(x[0], x[1])
    with open('./cal_result.txt', 'a+') as f:
        f.write(lines)


def save_integral_result(res):
    lines = "int res: %.8e\n"%res
    with open('./cal_result.txt', 'a+') as f:
        f.write(lines)


def cleanMisc(pathsimu):
    print('********** Cleaning the previous results **********')
    status = True

    log = os.sep.join([pathsimu, 'log' ])
    misc = os.sep.join([pathsimu, 'misc' ])
    output = os.sep.join([pathsimu, 'output' ])

    if os.path.isdir(log):
        if os.system('rm -r %s'%log) != 0:
            status = False
            print('> Impossible to delete log')
        else:
            print('> log deleted')
    else:
        print('> There is not a log')
    if os.path.isdir(misc):
        if os.system('rm -r %s'%misc) != 0:
            status = False
            print('> Impossible to delete misc')
        else:
            print('> misc deleted')
    else:
        print('> There is not a misc')
    if os.path.isdir(output):
        if os.system('rm -r %s'%output) != 0:
            status = False
            print('> Impossible to delete output')
        else:
            print('> output deleted')
    else:
        print('> There is not a output')
    print('****************************************************')
    return status


# usar una funcion para optimizar dentro de integralunction
# para validar que el metodo funciona correctamente
def run(x, stations_cells, df_data, new_stations_cells, new_data):
    val = 0
    ###### save parameters
    save_param(x)

    ###### update files lim
    print('''////////////////////////////////////////////////////
    x_in: %.9e, x_out: %.9e
****************************************************'''%(x[0], x[1]))
    updateBC(test_lim, x[0], x[1])

    ###### update files lim
    if cleanMisc(ss.dir_simu):
        ###### run sw2d
        command_line = '%s -i %s'%(ssw2d,test_in)
        status = os.system(command_line)
        if status == 0:
            ###### open simu results
            df_simu = ss.generateDFSimu(stations_cells, os.sep.join([ss.dir_simu, ss.misc]), ss.reference)
            new_simu = ss.generateDFSimu(new_stations_cells, os.sep.join([ss.dir_simu, ss.misc]), ss.reference)
            val = IntegralFuncion(df_data, df_simu, stations_cells, new_data, new_simu, new_stations_cells)
            save_integral_result(val)
        else:
            print('> Problem with the sw2d simulation')
    else:
        print('> imposible to clean the folders result')
    print('\n> integral result:', val)
    return val


def main():
    print('********** Open Config Calibration **********')

    ver, tri = ss.openMesh(os.sep.join([ss.dir_mesh, ss.mesh_name]))

    sta, g_sta = ss.openStations(ss.dir_data, ss.sta_name, ss.coor_name)

    x = sta['x_coord'].values
    y = sta['y_coord'].values

    # identify stations on the mesh
    cen, stations_cells = ss.identifyStations(ver, tri[:,1:], x, y)
    df_data = ss.openSStationsData(os.sep.join([ss.dir_data, ss.sda_name]))
    df_data = meanValueData(df_data)

    new_data, new_coor = ss.openNewData(os.sep.join([ss.dir_data, ss.data_name]), os.sep.join([ss.dir_data, ss.coor_name]))
    new_cen, new_stations_cells = ss.identifyStations(ver, tri[:,1:], new_coor['x'].values, new_coor['y'].values)
    new_data = meanValueNewData(new_data)


    print('********** Start Calibration **********')
    if backupBC():
        x0 = np.array([x_in, x_out])

        res = minimize(run, x0, args = (stations_cells,df_data, new_stations_cells, new_data), method='powell', options={'disp': True,'maxiter':100}, bounds=((0,5),(0,5)), tol=1e-1)

        print(res.x)


main()
